'use strict';

var svg2, tooltip2, biHiSankey2, path2, defs2, colorScale, highlightColorScale, isTransitioning2;

var OPACITY = {
    NODE_DEFAULT: 0.9,
    NODE_FADED: 0.1,
    NODE_HIGHLIGHT: 0.8,
    LINK_DEFAULT: 0.6,
    LINK_FADED: 0.05,
    LINK_HIGHLIGHT: 0.9
  },
  TYPES = ["LOCAL", "SERVICE","SERVICEPROVIDER0","SERVICEPROVIDER1","SERVICEPROVIDER2"],
  TYPE_COLORS = ["#66a61e", "#e6ab02", "#a6761d", "#8da0cb", "#fc8d62", "#ffd9ff"],
  TYPE_HIGHLIGHT_COLORS = ["#66c2a5", "#fc8d62", "#8da0cb", "#e78ac3", "#a6d854", "#ffd9ff"],
  LINK_COLOR = "#b3b3b3",
  INFLOW_COLOR = "#2E86D1",
  OUTFLOW_COLOR = "#D63028",
  NODE_WIDTH = 36,
  COLLAPSER = {
    RADIUS: NODE_WIDTH / 2,
    SPACING: 2
  },
  OUTER_MARGIN = 10,
  MARGIN = {
    TOP: 2 * (COLLAPSER.RADIUS + OUTER_MARGIN),
    RIGHT: OUTER_MARGIN,
    BOTTOM: OUTER_MARGIN,
    LEFT: OUTER_MARGIN
  },
  TRANSITION_DURATION = 400,
  HEIGHT = 600 - MARGIN.TOP - MARGIN.BOTTOM,
  WIDTH = 1700 - MARGIN.LEFT - MARGIN.RIGHT,
  LAYOUT_INTERATIONS = 3,
  REFRESH_INTERVAL = 7000;

var formatNumber = function (d) {
  var numberFormat = d3.format(",.0f"); // zero decimal places
  return " " + numberFormat(d/1024)+ " KB";
},


formatFlow = function (d) {
  var flowFormat = d3.format(",.0f"); // zero decimal places with sign
  return " "+ flowFormat(Math.abs(d/1024)) + (d < 0 ? " KB" : " KB");
},

// Used when temporarily disabling user interractions to allow animations to complete
disableUserInterractions2 = function (time) {
  isTransitioning2 = true;
  setTimeout(function(){
    isTransitioning2 = false;
  }, time);
},

hideTooltip2 = function () {
  return tooltip2.transition()
    .duration(TRANSITION_DURATION)
    .style("opacity", 0);
},

showTooltip2 = function () {
  return tooltip2
    .style("left", d3.event.pageX + "px")
    .style("top", d3.event.pageY + 15 + "px")
    .transition()
      .duration(TRANSITION_DURATION)
      .style("opacity", 1);
};

colorScale = d3.scale.ordinal().domain(TYPES).range(TYPE_COLORS),
highlightColorScale = d3.scale.ordinal().domain(TYPES).range(TYPE_HIGHLIGHT_COLORS),

svg2 = d3.select("#chart2").append("svg")
        .attr("width", WIDTH + MARGIN.LEFT + MARGIN.RIGHT)
        .attr("height", HEIGHT + MARGIN.TOP + MARGIN.BOTTOM)
      .append("g")
        .attr("transform", "translate(" + MARGIN.LEFT + "," + MARGIN.TOP + ")");

svg2.append("g").attr("id", "links");
svg2.append("g").attr("id", "nodes");
svg2.append("g").attr("id", "collapsers");

tooltip2 = d3.select("#chart2").append("div").attr("id", "tooltip2");

tooltip2.style("opacity", 0)
    .append("p")
      .attr("class", "value");

biHiSankey2 = d3.biHiSankey();

// Set the biHiSankey diagram properties
biHiSankey2
  .nodeWidth(NODE_WIDTH)
  .nodeSpacing(10)
  .linkSpacing(4)
  .arrowheadScaleFactor(0.5) // Specifies that 0.5 of the link's stroke WIDTH should be allowed for the marker at the end of the link.
  .size([WIDTH, HEIGHT]);

path2 = biHiSankey2.link().curvature(0.45);

defs2 = svg2.append("defs");

defs2.append("marker")
  .style("fill", LINK_COLOR)
  .attr("id", "arrowHead")
  .attr("viewBox", "0 0 6 10")
  .attr("refX", "1")
  .attr("refY", "5")
  .attr("markerUnits", "strokeWidth")
  .attr("markerWidth", "1")
  .attr("markerHeight", "1")
  .attr("orient", "auto")
  .append("path")
    .attr("d", "M 0 0 L 1 0 L 6 5 L 1 10 L 0 10 z");

defs2.append("marker")
  .style("fill", OUTFLOW_COLOR)
  .attr("id", "arrowHeadInflow")
  .attr("viewBox", "0 0 6 10")
  .attr("refX", "1")
  .attr("refY", "5")
  .attr("markerUnits", "strokeWidth")
  .attr("markerWidth", "1")
  .attr("markerHeight", "1")
  .attr("orient", "auto")
  .append("path")
    .attr("d", "M 0 0 L 1 0 L 6 5 L 1 10 L 0 10 z");

defs2.append("marker")
  .style("fill", INFLOW_COLOR)
  .attr("id", "arrowHeadOutlow")
  .attr("viewBox", "0 0 6 10")
  .attr("refX", "1")
  .attr("refY", "5")
  .attr("markerUnits", "strokeWidth")
  .attr("markerWidth", "1")
  .attr("markerHeight", "1")
  .attr("orient", "auto")
  .append("path")
    .attr("d", "M 0 0 L 1 0 L 6 5 L 1 10 L 0 10 z");

function update2() {
  var link, linkEnter, node, nodeEnter, collapser, collapserEnter;

  function dragmove(node) {
    node.x = Math.max(0, Math.min(WIDTH - node.width, d3.event.x));
    node.y = Math.max(0, Math.min(HEIGHT - node.height, d3.event.y));
    d3.select(this).attr("transform", "translate(" + node.x + "," + node.y + ")");
    biHiSankey2.relayout();
    svg.selectAll(".node").selectAll("rect").attr("height", function (d) { return d.height; });
    link.attr("d", path2);
  }

  function containChildren(node) {
    node.children.forEach(function (child) {
      child.state = "contained";
      child.parent = this;
      child._parent = null;
      containChildren(child);
    }, node);
  }

  function expand(node) {
    node.state = "expanded";
    node.children.forEach(function (child) {
      child.state = "collapsed";
      child._parent = this;
      child.parent = null;
      containChildren(child);
    }, node);
  }

  function collapse(node) {
    node.state = "collapsed";
    containChildren(node);
  }

  function restoreLinksAndNodes() {
    link
      .style("stroke", LINK_COLOR)
      .style("marker-end", function () { return 'url(#arrowHead)'; })
      .transition()
        .duration(TRANSITION_DURATION)
        .style("opacity", OPACITY.LINK_DEFAULT);

    node
      .selectAll("rect")
        .style("fill", function (d) {
          d.color = colorScale(d.type.replace(/ .*/, ""));
          return d.color;
        })
        .style("stroke", function (d) {
          return d3.rgb(colorScale(d.type.replace(/ .*/, ""))).darker(0.1);
        })
        .style("fill-opacity", OPACITY.NODE_DEFAULT);

    node.filter(function (n) { return n.state === "collapsed"; })
      .transition()
        .duration(TRANSITION_DURATION)
        .style("opacity", OPACITY.NODE_DEFAULT);
  }

  function showHideChildren(node) {
    disableUserInterractions2(2 * TRANSITION_DURATION);
    hideTooltip2();
    if (node.state === "collapsed") { expand(node); }
    else { collapse(node); }

    biHiSankey2.relayout();
    update();
    link.attr("d", path);
    restoreLinksAndNodes();
  }

  function highlightConnected(g) {
    link.filter(function (d) { return d.source === g; })
      .style("marker-end", function () { return 'url(#arrowHeadInflow)'; })
      .style("stroke", OUTFLOW_COLOR)
      .style("opacity", OPACITY.LINK_DEFAULT);

    link.filter(function (d) { return d.target === g; })
      .style("marker-end", function () { return 'url(#arrowHeadOutlow)'; })
      .style("stroke", INFLOW_COLOR)
      .style("opacity", OPACITY.LINK_DEFAULT);
  }

  function fadeUnconnected(g) {
    link.filter(function (d) { return d.source !== g && d.target !== g; })
      .style("marker-end", function () { return 'url(#arrowHead)'; })
      .transition()
        .duration(TRANSITION_DURATION)
        .style("opacity", OPACITY.LINK_FADED);

    node.filter(function (d) {
      return (d.name === g.name) ? false : !biHiSankey2.connected(d, g);
    }).transition()
      .duration(TRANSITION_DURATION)
      .style("opacity", OPACITY.NODE_FADED);
  }

  link = svg2.select("#links").selectAll("path.link")
    .data(biHiSankey2.visibleLinks(), function (d) { return d.id; });

  link.transition()
    .duration(TRANSITION_DURATION)
    .style("stroke-WIDTH", function (d) { return Math.max(1, d.thickness); })
    .attr("d", path2)
    .style("opacity", OPACITY.LINK_DEFAULT);


  link.exit().remove();


  linkEnter = link.enter().append("path")
    .attr("class", "link")
    .style("fill", "none");

  linkEnter.on('mouseenter', function (d) {
    if (!isTransitioning2) {
      showTooltip2().select(".value").text(function () {
        if (d.direction > 0) {
          return d.source.name + " → " + d.target.name + "\n" + formatNumber(d.value);
        }
        return d.target.name + " ← " + d.source.name + "\n" + formatNumber(d.value);
      });

      d3.select(this)
        .style("stroke", LINK_COLOR)
        .transition()
          .duration(TRANSITION_DURATION / 2)
          .style("opacity", OPACITY.LINK_HIGHLIGHT);
    }
  });

  linkEnter.on('mouseleave', function () {
    if (!isTransitioning2) {
      hideTooltip2();

      d3.select(this)
        .style("stroke", LINK_COLOR)
        .transition()
          .duration(TRANSITION_DURATION / 2)
          .style("opacity", OPACITY.LINK_DEFAULT);
    }
  });

  linkEnter.sort(function (a, b) { return b.thickness - a.thickness; })
    .classed("leftToRight", function (d) {
      return d.direction > 0;
    })
    .classed("rightToLeft", function (d) {
      return d.direction < 0;
    })
    .style("marker-end", function () {
      return 'url(#arrowHead)';
    })
    .style("stroke", LINK_COLOR)
    .style("opacity", 0)
    .transition()
      .delay(TRANSITION_DURATION)
      .duration(TRANSITION_DURATION)
      .attr("d", path2)
      .style("stroke-WIDTH", function (d) { return Math.max(1, d.thickness); })
      .style("opacity", OPACITY.LINK_DEFAULT);


  node = svg2.select("#nodes").selectAll(".node")
      .data(biHiSankey2.collapsedNodes(), function (d) { return d.id; });


  node.transition()
    .duration(TRANSITION_DURATION)
    .attr("transform", function (d) { return "translate(" + d.x + "," + d.y + ")"; })
    .style("opacity", OPACITY.NODE_DEFAULT)
    .select("rect")
      .style("fill", function (d) {
        d.color = colorScale(d.type.replace(/ .*/, ""));
        return d.color;
      })
      .style("stroke", function (d) { return d3.rgb(colorScale(d.type.replace(/ .*/, ""))).darker(0.1); })
      .style("stroke-WIDTH", "1px")
      .attr("height", function (d) { return d.height; })
      .attr("width", biHiSankey2.nodeWidth());


  node.exit()
    .transition()
      .duration(TRANSITION_DURATION)
      .attr("transform", function (d) {
        var collapsedAncestor, endX, endY;
        collapsedAncestor = d.ancestors.filter(function (a) {
          return a.state === "collapsed";
        })[0];
        endX = collapsedAncestor ? collapsedAncestor.x : d.x;
        endY = collapsedAncestor ? collapsedAncestor.y : d.y;
        return "translate(" + endX + "," + endY + ")";
      })
      .remove();


  nodeEnter = node.enter().append("g").attr("class", "node");

  nodeEnter
    .attr("transform", function (d) {
      var startX = d._parent ? d._parent.x : d.x,
          startY = d._parent ? d._parent.y : d.y;
      return "translate(" + startX + "," + startY + ")";
    })
    .style("opacity", 1e-6)
    .transition()
      .duration(TRANSITION_DURATION)
      .style("opacity", OPACITY.NODE_DEFAULT)
      .attr("transform", function (d) { return "translate(" + d.x + "," + d.y + ")"; });

  nodeEnter.append("text");
  nodeEnter.append("rect")
    .style("fill", function (d) {
      d.color = colorScale(d.type.replace(/ .*/, ""));
      return d.color;
    })
    .style("stroke", function (d) {
      return d3.rgb(colorScale(d.type.replace(/ .*/, ""))).darker(0.1);
    })
    .style("stroke-WIDTH", "1px")
    .attr("height", function (d) { return d.height; })
    .attr("width", biHiSankey2.nodeWidth());

  node.on("mouseenter", function (g) {
    if (!isTransitioning2) {
      restoreLinksAndNodes();
      highlightConnected(g);
      fadeUnconnected(g);

      d3.select(this).select("rect")
        .style("fill", function (d) {
          d.color = d.netFlow > 0 ? INFLOW_COLOR : OUTFLOW_COLOR;
          return d.color;
        })
        .style("stroke", function (d) {
          return d3.rgb(d.color).darker(0.1);
        })
        .style("fill-opacity", OPACITY.LINK_DEFAULT);

      tooltip2
        .style("left", g.x + MARGIN.LEFT + "px")
        .style("top", g.y + g.height + MARGIN.TOP + 485 + "px")
        .transition()
          .duration(TRANSITION_DURATION)
          .style("opacity", 1).select(".value")
          .text(function () {
            var additionalInstructions = g.children.length ? "\n(Double click to expand)" : "";
            return g.name + "\nNet flow: " + formatFlow(g.netFlow) + additionalInstructions;
          });
    }
  });

  node.on("mouseleave", function () {
    if (!isTransitioning2) {
      hideTooltip2();
      restoreLinksAndNodes();
    }
  });

  node.filter(function (d) { return d.children.length; })
    .on("dblclick", showHideChildren);

  // allow nodes to be dragged to new positions
  node.call(d3.behavior.drag()
    .origin(function (d) { return d; })
    .on("dragstart", function () { this.parentNode.appendChild(this); })
    .on("drag", dragmove));

  // add in the text for the nodes
  node.filter(function (d) { return d.value !== 0; })
    .select("text")
      .attr("x", -6)
      .attr("y", function (d) { return d.height / 2; })
      .attr("dy", ".35em")
      .attr("text-anchor", "end")
      .attr("transform", null)
      .text(function (d) { return d.name; })
    .filter(function (d) { return d.x < WIDTH / 2; })
      .attr("x", 6 + biHiSankey2.nodeWidth())
      .attr("text-anchor", "start");


  collapser = svg2.select("#collapsers").selectAll(".collapser")
    .data(biHiSankey2.expandedNodes(), function (d) { return d.id; });


  collapserEnter = collapser.enter().append("g").attr("class", "collapser");

  collapserEnter.append("circle")
    .attr("r", COLLAPSER.RADIUS)
    .style("fill", function (d) {
      d.color = colorScale(d.type.replace(/ .*/, ""));
      return d.color;
    });

  collapserEnter
    .style("opacity", OPACITY.NODE_DEFAULT)
    .attr("transform", function (d) {
      return "translate(" + (d.x + d.width / 2) + "," + (d.y + COLLAPSER.RADIUS) + ")";
    });

  collapserEnter.on("dblclick", showHideChildren);

  collapser.select("circle")
    .attr("r", COLLAPSER.RADIUS);

  collapser.transition()
    .delay(TRANSITION_DURATION)
    .duration(TRANSITION_DURATION)
    .attr("transform", function (d, i) {
      return "translate("
        + (COLLAPSER.RADIUS + i * 2 * (COLLAPSER.RADIUS + COLLAPSER.SPACING))
        + ","
        + (-COLLAPSER.RADIUS - OUTER_MARGIN)
        + ")";
    });

  collapser.on("mouseenter", function (g) {
    if (!isTransitioning2) {
      showTooltip2().select(".value")
        .text(function () {
          return g.name + "\n(Double click to collapse)";
        });

      var highlightColor = highlightColorScale(g.type.replace(/ .*/, ""));

      d3.select(this)
        .style("opacity", OPACITY.NODE_HIGHLIGHT)
        .select("circle")
          .style("fill", highlightColor);

      node.filter(function (d) {
        return d.ancestors.indexOf(g) >= 0;
      }).style("opacity", OPACITY.NODE_HIGHLIGHT)
        .select("rect")
          .style("fill", highlightColor);
    }
  });

  collapser.on("mouseleave", function (g) {
    if (!isTransitioning2) {
      hideTooltip2();
      d3.select(this)
        .style("opacity", OPACITY.NODE_DEFAULT)
        .select("circle")
          .style("fill", function (d) { return d.color; });

      node.filter(function (d) {
        return d.ancestors.indexOf(g) >= 0;
      }).style("opacity", OPACITY.NODE_DEFAULT)
        .select("rect")
          .style("fill", function (d) { return d.color; });
    }
  });

  collapser.exit().remove();

}

$(document).ready(function() {
    ajax_call2();
});

var ajax_call2 = function() {
 $.ajax({
        url: "http://localhost:5000/tcpin"
	,method: "GET"
    }).then(function(data) {
        $.ajax({
            url: "http://localhost:5000/udpin"
        ,method: "GET"
        }).then(function(data2) {
            jQuery.merge(data._items,data2._items);
            var result = process2(data);
            draw2(result);
        });
    });
};

var list = $(".test");
function process2(data) {

	var classes = new Array();
	let serviceNodes = [];
	var spNodes = [];
	var spNodesTrack = [];
	var slinks = {};
	var splinks = {};
	slinks['LOCAL'] = {};
	$.each(data._items, function(i, item){
		var service = String(item._id.service);
		if( $.inArray(service,serviceNodes) == -1){
			 serviceNodes.push(service);
			 slinks[service] = {};
              slinks["LOCAL"][service]= {"ccount": item.ccount, "scount" :item.scount };
		}else{
            slinks['LOCAL'][service].ccount +=  item.ccount;
            slinks['LOCAL'][service].scount +=  item.scount;
		}



		var ssp = item._id.sp+ '';
        var sps = ssp.split(',');

        if((slinks[service][sps[0]] in slinks) == false){
           slinks[service][sps[0]] = {"ccount": item.ccount, "scount" :item.scount };
        }else{
            slinks[service][[sps[0]]].ccount +=  item.ccount;
            slinks[service][[sps[0]]].scount +=  item.scount;
        }

        for(i = 0; i< sps.length; i++){
            var index = $.inArray(sps[i],spNodesTrack);
            if( index == -1){
                spNodes.push({"name":sps[i], "order":i});
                spNodesTrack.push(sps[i]);
            }else{

                if(spNodes[index].order < i){
                    spNodes[index].order = i;
                }
            }

            //there is no link from the last node
            if(i >= sps.length-1 ){
                continue;
            }

            if((sps[i] in splinks) == false){
		        splinks[sps[i]] = {};
		    }
            if((splinks[sps[i]][i+1] in splinks) == false){
               var splink = {}
               splink[sps[i+1]] = {"ccount": item.ccount, "scount" :item.scount};
               splinks[sps[i]] = splink;
            }else{
                splinks[sps[i]][[sps[i+1]]].ccount +=  item.ccount;
                splinks[sps[i]][[sps[i+1]]].scount +=  item.scount;
            }

        }


	});
   serviceNodes.push('LOCAL');
	var result = {"serviceNodes": serviceNodes, "spNodes": spNodes, "sLinks": slinks, "spLinks": splinks};
	return result;
}

function draw2(data){
    var nodes = [];
    let serviceNodes = data['serviceNodes'].reverse();
    for(var i =0; i < serviceNodes.length; i++){
        var parentNode = 'LOCAL';
        var type = 'SERVICE';
        if(serviceNodes[i] == 'LOCAL'){
            parentNode = null;
            type = 'LOCAL'
        }
        nodes.push(  {"type":type,"id":"ins_"+serviceNodes[i],"parent":parentNode,"name":serviceNodes[i]});
    }

    var maxOrder = 2;
    //perform for each order for correct hirarchy
    for(var j = 0; j <= maxOrder; j++){
        for(var node of data['spNodes']){
            if(node.order == j){
                nodes.push(  {"type":"SERVICEPROVIDER"+String(node.order),"id":"in_"+node.name,"parent":null,"name":node.name});
            }
        }
    }

    var links = [];
    $.each(data['sLinks'], function(index, target){
        var prefix = "in_";
        if(index == 'LOCAL'){
            prefix = "ins_";
        }
        $.each(target, function(index2, link){
            links.push( {"source":"ins_"+index, "target":prefix+index2, "value":link['ccount']});
            links.push( {"source":prefix+index2, "target":"ins_"+index, "value":link['scount']});
        });
    });
    $.each(data['spLinks'], function(index, target){
        $.each(target, function(index2, link){
            links.push( {"source":"in_"+index, "target":"in_"+index2, "value":link['ccount']});
            links.push( {"source":"in_"+index, "target":"in_"+index2, "value":link['ccount']});
            links.push( {"source":"in_"+index2, "target":"in_"+index, "value":link['scount']});
        });
    });

    biHiSankey2
      .nodes(nodes)
     .links(links)
      .initializeNodes(function (node) {
        node.state = node.parent ? "contained" : "collapsed";
      })
      .nodeSpacing(40)
      .layout(100000)
    ;

    disableUserInterractions2(2 * TRANSITION_DURATION);

    update2();
}

var interval = 1000 * 60 * 0.30;
//setInterval(ajax_call2, interval);