$(document).ready(function() {
    $.ajax({
        url: "http://localhost:5000/tcpout"
	,method: "GET"
    }).then(function(data) {
        process(data);
    });
});

function unique(arr) {
    var u = {}, a = [];
    for(var i = 0, l = arr.length; i < l; ++i){
        if(!u.hasOwnProperty(arr[i])) {
            a.push(arr[i]);
            u[arr[i]] = 1;
        }
    }
    return a;
}

function process(data) {
var list = $(".test");
	var classes = new Array();
	var serviceNodes = {};
	var spNodes = new Set();
	var slinks = {};
	var splinks = {};
	slinks['LOCAL'] = {};
	serviceNodes['LOCAL'] = 1;
	$.each(data._items, function(i, item){
		service = item._id.service;
		if( (service in serviceNodes) == false){
			 serviceNodes[service] = 1;
			 slinks[service] = {};
             slinks["LOCAL"][service]= {"ccount": item.ccount, "scount" :item.scount };
		}else{
            slinks["LOCAL"][service].ccount +=  item.ccount;
            slinks["LOCAL"][service].scount +=  item.scount;
		}

		var ssp = item._id.sp+ '';
        sps = ssp.split(',').reverse();

        if((slinks[service][sps[0]] in slinks) == false){
           slinks[service][sps[0]] = {"ccount": item.ccount, "scount" :item.scount };
        }else{
            slinks[service][[sps[0]]].ccount +=  item.ccount;
            slinks[service][[sps[0]]].scount +=  item.scount;
        }

        for(i =0; i<sps.length; i++){
            spNodes.add(sps[i]);

            //there is no link from the last node
            if(i >= sps.length-1 ){
                continue;
            }

            if((sps[i] in splinks) == false){
		        splinks[sps[i]] = {};
		    }
            if((splinks[sps[i]][i+1] in splinks) == false){
               splink = {}
               splink[sps[i+1]] = {"ccount": item.ccount, "scount" :item.scount };
               splinks[sps[i]] = splink;
            }else{
                splinks[sps[i]][[sps[i+1]]].ccount +=  item.ccount;
                splinks[sps[i]][[sps[i+1]]].scount +=  item.scount;
            }

        }


	});
	$.each(slinks, function(index, link){
		$.each(link, function(ind, l){
			$(list).append('<li>' +'service links: '+ index+'-' + ind + ' ' + JSON.stringify(l)+ '</li>');
		});
	});
	$.each(serviceNodes, function(index, service){
		$(list).append('<li>' + 'service nodes: ' + index + ' ' + service + '</li>');
	});
	for(item of spNodes){
	    $(list).append('<li>' +'sp nodes: ' + item + '</li>');
	}
}

