X_DOMAINS = '*'
PAGINATION = False

DOMAIN = {'udpsessionsv4':{},
        'tcpout':{
            'datasource':{
                'source': 'tcpsessionsv4',
                'aggregation' : {
                    'pipeline': [
                        {"$match" : { 
                            "$and" : [
                                { "direction" : 1 }, 
                                { "active" : 1 }
                           ]
                          } 
                        },
                        { "$unwind" : "$classification.SERVICE" },
                        {"$group" : {
                            "_id": {
                                "service" :  "$classification.SERVICE",
                                "sp" : "$classification.SERVICEPROVIDER",
                                },
                            "ccount" : {"$sum" : "$client.datatransfered" },
                            "scount" : {"$sum" : "$server.datatransfered" }
                            }}
                        ]
                    }

                }
            },
        'tcpin':{
            'datasource':{
                'source': 'tcpsessionsv4',
                'aggregation' : {
                    'pipeline': [
                        {"$match" : {
                            "$and" : [
                                { "direction" : 0 },
                                { "active" : 1 }
                           ]
                          }
                        },
                        { "$unwind" : "$classification.SERVICE" },
                        {"$group" : {
                            "_id": {
                                "service" :  "$classification.SERVICE",
                                "sp" : "$classification.SERVICEPROVIDER",
                                },
                            "ccount" : {"$sum" : "$client.datatransfered" },
                            "scount" : {"$sum" : "$server.datatransfered" }
                            }}
                        ]
                    }

                }
            },
        'udpout':{
            'datasource':{
                'source': 'udpsessionsv4',
                'aggregation' : {
                    'pipeline': [
                        {"$match" : {
                            "$and" : [
                                { "direction" : 1 },
                                { "active" : 1 }
                           ]
                          }
                        },
                        { "$unwind" : "$classification.SERVICE" },
                        {"$group" : {
                            "_id": {
                                "service" :  "$classification.SERVICE",
                                "sp" : "$classification.SERVICEPROVIDER",
                                },
                            "ccount" : {"$sum" : "$client.datatransfered" },
                            "scount" : {"$sum" : "$server.datatransfered" }
                            }}
                        ]
                    }

                }
            },
        'udpin':{
            'datasource':{
                'source': 'udpsessionsv4',
                'aggregation' : {
                    'pipeline': [
                        {"$match" : {
                            "$and" : [
                                { "direction" : 0 },
                                { "active" : 1 }
                           ]
                          }
                        },
                        { "$unwind" : "$classification.SERVICE" },
                        {"$group" : {
                            "_id": {
                                "service" :  "$classification.SERVICE",
                                "sp" : "$classification.SERVICEPROVIDER",
                                },
                            "ccount" : {"$sum" : "$client.datatransfered" },
                            "scount" : {"$sum" : "$server.datatransfered" }
                            }}
                        ]
                    }

                }
            }
        }
MONGO_HOST = 'localhost'
MONGO_PORT = 27017
MONGO_DBNAME = 'connclassifier'
